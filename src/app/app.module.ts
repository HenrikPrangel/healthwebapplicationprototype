import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './core/app-routing.module';
import {CustomMaterialModule} from './core/material.module';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from "@angular/forms";

import {AppComponent} from './app.component';
import {LoginComponent} from './components/login/login.component';
import {SingleViewComponent} from './components/plan/single-view/single-view.component';
import {NavigationBarComponent} from './components/general/navigation-bar/navigation-bar.component';
import {MultiViewComponent} from './components/plan/multi-view/multi-view.component';
import {DashboardComponent} from './components/general/dashboard/dashboard.component';
import {DiaryComponent} from './components/diary/diary.component';
import {CreationComponent} from './components/plan/creation/creation.component';
import {PlanCardComponent} from './components/general/plan-card/plan-card.component';
import {NotImplementedComponent} from './components/general/not-implemented/not-implemented.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SingleViewComponent,
    NavigationBarComponent,
    MultiViewComponent,
    DashboardComponent,
    DiaryComponent,
    CreationComponent,
    PlanCardComponent,
    NotImplementedComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    CustomMaterialModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
