export class PlanElement {
  planElementId: number;
  elementType: string;
  elementId: number;
  elementName: string;
  amount: number;
  userValidated: boolean;
}
