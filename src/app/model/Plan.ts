import {Section} from "./Section";

export class Plan {
  id: number;
  name: string;
  description: string;
  acceccibleToPublic: boolean;
  userId: number;
  planSectionList: Section[];
}
