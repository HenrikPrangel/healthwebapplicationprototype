export class User {
  id: number;
  userName: string;
  password: string;
  activePlan: number;
}
