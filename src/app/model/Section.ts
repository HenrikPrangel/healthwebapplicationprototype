import {Day} from "./Day";

export class Section {
  id: number;
  planId: number;
  sectionName: string;
  planDayList: Day[];
}
