import {DayTimeFrame} from "./DayTimeFrame";

export class Day {
  planDayId: number;
  planDaySectionId;
  day: string;
  dayTimeFrameList: DayTimeFrame[];
}
