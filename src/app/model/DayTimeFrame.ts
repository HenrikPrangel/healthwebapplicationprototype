import {PlanElement} from "./PlanElement";

export class DayTimeFrame {
  dayTimeFrameId: number;
  dayTimeFrameName: string;
  planElementList: PlanElement[];
}
