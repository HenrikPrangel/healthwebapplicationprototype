import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DashboardComponent} from "../components/general/dashboard/dashboard.component";
import {MultiViewComponent} from "../components/plan/multi-view/multi-view.component";
import {CreationComponent} from "../components/plan/creation/creation.component";
import {SingleViewComponent} from "../components/plan/single-view/single-view.component";
import {NotImplementedComponent} from "../components/general/not-implemented/not-implemented.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: {
      title: 'Dashboard'
    }
  },
  {
    path: 'userPlans',
    component: MultiViewComponent,
    data: {
      title: 'User Plans'
    }
  },
  {
    path: 'publicPlans',
    component: MultiViewComponent,
    data: {
      title: 'Public Plans'
    }
  },
  {
    path: 'planCreation',
    component: CreationComponent,
    data: {
      title: 'Plan Creation'
    }
  },
  {
    path: 'planView/:id',
    component: SingleViewComponent,
    data: {
      title: 'Plan Details'
    }
  },
  {
    path: 'notImplemented',
    component: NotImplementedComponent,
    data: {
      title: 'Not Implemented Yet'
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
