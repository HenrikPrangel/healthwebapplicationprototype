import {HttpHeaders} from "@angular/common/http";

export class CONFIG {
  static Url = 'http://localhost:8080';
  static HttpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };
}
