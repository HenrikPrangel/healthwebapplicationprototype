import {Component, Input, OnInit} from '@angular/core';
import {Day} from "../../model/Day";
import {MatIconRegistry} from "@angular/material";
import {DomSanitizer} from "@angular/platform-browser";
import {PlanElement} from "../../model/PlanElement";

@Component({
  selector: 'app-diary',
  templateUrl: './diary.component.html',
  styleUrls: ['./diary.component.css']
})
export class DiaryComponent implements OnInit {
  currentDay: Number;
  @Input() day: Day;

  constructor(
    private matIconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.currentDay = new Date().getDay();
    console.log(this.currentDay);
    console.log(this.day);
    this.RegisterIcons();
  }

  RegisterIcons() {
    this.matIconRegistry.addSvgIcon(
      'checkmark',
      this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/checkmark.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'cross',
      this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/cross.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'menu',
      this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/menu.svg')
    );
  }

  RemoveElement(elementId: number, dayTimeFrameId: number) {
    let planelement: PlanElement =
      this.day.dayTimeFrameList.find(dtf => dtf.dayTimeFrameId === dayTimeFrameId)
        .planElementList.find(pe => pe.elementId === elementId);

    this.day.dayTimeFrameList.find(dtf => dtf.dayTimeFrameId === dayTimeFrameId)
      .planElementList.splice(this.day.dayTimeFrameList.find(dtf => dtf.dayTimeFrameId === dayTimeFrameId)
      .planElementList.indexOf(planelement), 1);
  }
}
