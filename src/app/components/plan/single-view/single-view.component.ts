import {Component, OnInit} from '@angular/core';
import {Plan} from "../../../model/Plan";
import {ActivatedRoute} from "@angular/router";
import {PlanService} from "../../../services/plan.service";
import {NavigationService} from "../../../services/navigation.service";
import {GlobalService} from "../../../services/global.service";

@Component({
  selector: 'app-single-view',
  templateUrl: './single-view.component.html',
})
export class SingleViewComponent implements OnInit {
  plan: Plan;
  planId: String;
  isUserPlan: boolean;
  loaded: boolean;

  constructor(
    private route: ActivatedRoute,
    private planService: PlanService,
    private navigationService: NavigationService,
    private globalService: GlobalService) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.planId = params.get("id");
      if (this.planId) {
        this.planService.GetPlanById(Number(this.planId)).subscribe(
          data => {
            this.plan = data;
            this.isUserPlan = data.userId === this.globalService.currentUser.id;

            this.loaded = true;
          }
        );
      } else {
        this.loaded = true;
      }
    })
  }

  ChoosePlan() {
    this.planService.SetActivePlanForUser(this.plan.id).subscribe(
      data => {
        this.globalService.currentUser = data;
        this.navigationService.NavigateToDashBoard();
      }
    )
  }

}
