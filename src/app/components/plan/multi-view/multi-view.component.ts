import {Component, OnInit} from '@angular/core';
import {GlobalService} from "../../../services/global.service";
import {PlanService} from "../../../services/plan.service";
import {NavigationService} from "../../../services/navigation.service";
import {Plan} from "../../../model/Plan";

@Component({
  selector: 'app-multi-view',
  templateUrl: './multi-view.component.html',
})
export class MultiViewComponent implements OnInit {
  userPlans: [Plan];
  isSearch: boolean;
  loaded: boolean;

  constructor(
    private navigationService: NavigationService,
    private  globalService: GlobalService,
    private planService: PlanService) {
  }

  ngOnInit() {
    this.isSearch = this.navigationService.getCurrentRout() === '/publicPlans';

    if (!this.isSearch) {
      this.GetUserPlans();
    } else {
      this.GetPublicPlans()
    }
  }

  GetUserPlans() {
    this.planService.GetAllUserPlans().subscribe(
      data => {
        this.userPlans = data;
        this.loaded = true;
      },
      error => {
      });
  };

  GetPublicPlans() {
    this.planService.GetAllPublicPlans().subscribe(
      data => {
        this.userPlans = data;
        this.loaded = true;
      },
      error => {
      });
  };

  GoToPlanCreation() {
    this.navigationService.NavigateToPlanCreation();
  }

}
