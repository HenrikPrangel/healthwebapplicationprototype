import {Component, OnInit} from '@angular/core';
import {Plan} from "../../../model/Plan";
import {Section} from "../../../model/Section";
import {Day} from "../../../model/Day";
import {Days} from "../../../model/enums/Days";
import {PlanService} from "../../../services/plan.service";
import {NavigationService} from "../../../services/navigation.service";
import {DayTimeFrame} from "../../../model/DayTimeFrame";

@Component({
  selector: 'app-creation',
  templateUrl: './creation.component.html',
})
export class CreationComponent implements OnInit {
  plan: Plan;

  constructor(
    private planService: PlanService,
    private navigationService: NavigationService) {
    this.InitNewPlan();
  }

  ngOnInit() {
  }

  InitNewPlan() {
    this.plan = new Plan();
    this.plan.acceccibleToPublic = false;
    let initialSection = this.CreateNewSection('Base');
    this.plan.planSectionList = [initialSection];
  }

  CreateNewSection(sectionName: string): Section {
    let initialSection: Section = new Section();
    initialSection.sectionName = 'sectionName';
    let mon = new Day();
    mon.day = Days.Monday;
    mon.dayTimeFrameList = this.GetNewDefaultDayTimeFrameList();
    let tue = new Day();
    tue.day = Days.Tuesday;
    tue.dayTimeFrameList = this.GetNewDefaultDayTimeFrameList();
    let wed = new Day();
    wed.day = Days.Wednesday;
    wed.dayTimeFrameList = this.GetNewDefaultDayTimeFrameList();
    let thu = new Day();
    thu.day = Days.Thursday;
    thu.dayTimeFrameList = this.GetNewDefaultDayTimeFrameList();
    let fri = new Day();
    fri.day = Days.Friday;
    fri.dayTimeFrameList = this.GetNewDefaultDayTimeFrameList();
    let sat = new Day();
    sat.day = Days.Saturday;
    sat.dayTimeFrameList = this.GetNewDefaultDayTimeFrameList();
    let sun = new Day();
    sun.day = Days.Sunday;
    sun.dayTimeFrameList = this.GetNewDefaultDayTimeFrameList();

    initialSection.planDayList = [mon, tue, wed, thu, fri, sat, sun];
    return initialSection;
  }

  CreatePlan() {
    this.planService.CreateOrUpdatePlan(this.plan).subscribe(
      data => {
        if (data) {
          this.navigationService.NavigateToPlanDetails(data.id);
        }
      }
    );
  }

  GetNewDefaultDayTimeFrameList(): DayTimeFrame[] {
    let breakfast: DayTimeFrame = this.GetNewDayTimeFrame('Breakfast');
    let supper: DayTimeFrame = this.GetNewDayTimeFrame('Supper');
    let lunch: DayTimeFrame = this.GetNewDayTimeFrame('Lunch');
    let training: DayTimeFrame = this.GetNewDayTimeFrame('Training');
    return [breakfast, supper, lunch, training];
  }

  GetNewDayTimeFrame(name: string) {
    let dayTimeFrame = new DayTimeFrame();
    dayTimeFrame.dayTimeFrameName = name;
    dayTimeFrame.planElementList = [];
    return dayTimeFrame;
  }

}
