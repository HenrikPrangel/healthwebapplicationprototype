import {Component, EventEmitter, Output} from '@angular/core';
import {LoginService} from '../../services/login.service';
import {GlobalService} from '../../services/global.service';
import {NavigationService} from "../../services/navigation.service";
import {User} from "../../model/User";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent {
  activeUser: User;
  model: any = {};
  errorMessage: string;
  @Output() onLogin: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private navigationService: NavigationService,
    private loginService: LoginService,
    private  globalService: GlobalService) {
  }

  ngOnInit() {
  }

  login() {
    this.loginService.Login(this.model).subscribe(
      data => {
        if (data) {
          this.activeUser = data;
          this.globalService.currentUser = data;
          this.onLogin.emit(data);
          this.navigationService.NavigateToDashBoard()
        }
      },
      error => {
        this.errorMessage = error.message;
      });
  };

}
