import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NavigationService} from "../../../services/navigation.service";
import {LoginService} from "../../../services/login.service";
import {GlobalService} from "../../../services/global.service";
import {User} from "../../../model/User";
import {MatIconRegistry} from "@angular/material";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css']
})
export class NavigationBarComponent implements OnInit {
  @Input() showOnlyTitle: boolean;
  @Input() user: User;
  @Output() logout: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private navigationService: NavigationService,
    private loginService: LoginService,
    private globalService: GlobalService,
    private matIconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.RegisterIcons();
  }

  Logout() {
    this.loginService.Logout();
    this.globalService.currentUser = null;
    this.logout.emit();
  }

  RegisterIcons() {
    this.matIconRegistry.addSvgIcon(
      'user-default',
      this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/user_default.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'omega',
      this.sanitizer.bypassSecurityTrustResourceUrl('assets/icons/omega.svg')
    );
  }

}
