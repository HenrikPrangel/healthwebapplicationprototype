import {Component, OnInit} from '@angular/core';
import {GlobalService} from "../../../services/global.service";
import {PlanService} from "../../../services/plan.service";
import {Plan} from "../../../model/Plan";
import {NavigationService} from "../../../services/navigation.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {
  activePlan: Plan;
  loaded: boolean;

  constructor(
    private navigationService: NavigationService,
    private  globalService: GlobalService,
    private planService: PlanService) {
  }

  ngOnInit() {
    this.FetchActivePlanIfExists();
  }

  FetchActivePlanIfExists() {
    if (!this.globalService.currentUser.activePlan) {
      this.loaded = true;
      return;
    }
    this.planService.GetPlanById(this.globalService.currentUser.activePlan).subscribe(
      data => {
        if (data) {
          this.globalService.activePlan = data;
          this.activePlan = data;
          this.loaded = true;
        }
      },
      error => {
      });
  }

  GoToMyPlans() {
    this.navigationService.NavigateToUserPlans();
  }

  GoToPublicPlans() {
    this.navigationService.NavigateToPublicPlans();
  }

  GetDayPlan() {
    if (!this.activePlan.planSectionList || !this.activePlan.planSectionList[0] || !this.activePlan.planSectionList[0].planDayList) {
      return;
    }
    return this.activePlan.planSectionList[0].planDayList.find(day => day.day === this.GetCurrentDay());
  }

  GetCurrentDay() {
    let today = new Date().getDay();

    switch (today) {
      case 1 :
        return 'Monday';
        break;
      case 2 :
        return 'Tuesday';
        break;
      case 3 :
        return 'Wednesday';
        break;
      case 4 :
        return 'Thursday';
        break;
      case 5 :
        return 'Friday';
        break;
      case 6 :
        return 'Saturday';
        break;
      case 0 :
        return 'Sunday';
        break;
    }
  }

}
