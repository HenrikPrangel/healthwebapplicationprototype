import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Plan} from "../../../model/Plan";

@Component({
  selector: 'app-plan-card',
  templateUrl: './plan-card.component.html',
})
export class PlanCardComponent implements OnInit {
  @Input() plan: Plan;
  @Input() showButton: boolean;
  @Input() isUserPlan: boolean;
  @Output() buttonClick: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit() {
  }

  Click() {
    this.buttonClick.emit();
  }

}
