import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from "../model/User";
import {CONFIG} from "../core/conf"

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) {
  }

  Login(user: User) {
    return this.http.post<User>(CONFIG.Url + '/login', user, CONFIG.HttpOptions);
  }

  Logout() {
    return this.http.get(CONFIG.Url + '/logout', CONFIG.HttpOptions);
  }

}
