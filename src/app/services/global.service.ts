import {Injectable} from '@angular/core';
import {User} from "../model/User";
import {Plan} from "../model/Plan";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  private _currentUser: User;
  private _activePlan: Plan;

  constructor(private http: HttpClient) {
  }

  get currentUser(): User {
    return this._currentUser;
  }

  set currentUser(value: User) {
    this._currentUser = value;
  }

  get activePlan(): Plan {
    return this._activePlan;
  }

  set activePlan(value: Plan) {
    this._activePlan = value;
  }

}
