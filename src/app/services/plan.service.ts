import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CONFIG} from "../core/conf"
import {Plan} from "../model/Plan";
import {User} from "../model/User";

@Injectable({
  providedIn: 'root'
})
export class PlanService {

  constructor(private http: HttpClient) {
  }

  GetAllUserPlans() {
    return this.http.get<[Plan]>(CONFIG.Url + '/plan', CONFIG.HttpOptions);
  }

  GetAllPublicPlans() {
    return this.http.get<[Plan]>(CONFIG.Url + '/plan/public', CONFIG.HttpOptions);
  }

  GetPlanById(id: number) {
    return this.http.get<Plan>(CONFIG.Url + '/plan/' + id, CONFIG.HttpOptions);
  }

  SetActivePlanForUser(id: number) {
    return this.http.post<User>(CONFIG.Url + '/setActivePlanForUser/' + id, CONFIG.HttpOptions);
  }

  CreateOrUpdatePlan(plan: Plan) {
    return this.http.post<Plan>(CONFIG.Url + '/plan', plan, CONFIG.HttpOptions);
  }

}
