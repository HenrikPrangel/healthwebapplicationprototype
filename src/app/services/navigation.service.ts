import {Injectable} from '@angular/core';
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  constructor(private router: Router) {
  }

  getCurrentRout() {
    return this.router.url;
  }

  NavigateToDashBoard() {
    return this.router.navigate(['/dashboard'])
  }

  NavigateToPlanCreation() {
    return this.router.navigate(["/planCreation"])
  }

  NavigateToUserPlans() {
    return this.router.navigate(["/userPlans"])
  }

  NavigateToPublicPlans() {
    return this.router.navigate(["/publicPlans"])
  }

  NavigateToLogin() {
    return this.router.navigate(["/login"])
  }

  NavigateToPlanDetails(id: number) {
    return this.router.navigate(["/planView/" + id])
  }

  NavigateToNotImplemented() {
    return this.router.navigate(["/notImplemented"])
  }

}
