import {Component} from '@angular/core';
import {User} from "./model/User";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  activeUser: User;

  constructor() {
  }

  ngOnInit() {
  }

  updateUser(user: User) {
    this.activeUser = user;
  }
}
