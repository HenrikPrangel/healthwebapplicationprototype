# ModelBasedHealthApp

##This is the prototype client of the "Model based health application"

* For this client to work properly, the prototype web service needs to be run aswell, on port 8080
* Log in process is simulated, and correct users that can be logged in with are: {"userName": "user"}, {"userName": "user1"}, {"userName": "user2"} (No passwords are needed)
* This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

##Implemented/Depicted processes of the prototype client

* Choosing a "health plan" for user account
* Prefilling of daily diary based on chosen "health plan"
* Validating diary elements
* Looking at user owned/created "health plans"
* Looking at public "health plans"
* Opening a detailed view of a "health plan"
* Browsing the "health plan" model within the detailed view - looking at specific days and elements of those days

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
